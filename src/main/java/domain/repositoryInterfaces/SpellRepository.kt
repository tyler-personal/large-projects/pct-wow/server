package domain.repositoryInterfaces

import domain.entities.Spell

interface SpellRepository {
    fun getSpell(id: String): Spell?
}