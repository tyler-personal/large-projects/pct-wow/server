import io.javalin.Javalin
import presentation.controllers.*

object Main { // This has to be here for Maven & Heroku
    @JvmStatic fun main(args: Array<String>) {
        val app = Javalin.create().apply {
            port(ProcessBuilder().environment()["PORT"]?.toInt() ?: 7000)
            enableStaticFiles(viewLocation)
        }.start()

        app.get("/", MainController::index)

        app.get("/groups/:id", GroupsController::show)
        app.ws("/groups/:id", GroupsController::join)
        app.post("/groups/create", GroupsController::create)

        app.get("/groups/:id/exists", GroupsController::checkIfGroupExists)
        app.post("/groups/:id/spells", GroupsController::trackNewSpell)
        app.post("/groups/:id/players", GroupsController::trackNewPlayer)
        app.post("/groups/:id/spellTriggered", GroupsController::spellTriggered)

        app.ws("/chat/:name", ChatController::join)
    }
}

