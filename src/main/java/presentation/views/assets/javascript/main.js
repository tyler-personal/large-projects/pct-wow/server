function setupModal(inputID, modalID, modalButtonID, submitButtonID, submitAction) {
    let input = $("#" + inputID);
    let modal = $("#" + modalID);
    let modalButton = $("#" + modalButtonID);
    let submitButton = $("#" + submitButtonID);

    input.on("change paste keyup", () => {
        if (input.val().length > 0)
            submitButton.removeClass("disabled");
        else
            submitButton.addClass("disabled");
    });

    modalButton.on('click', () => modal.modal("show"));
    submitButton.on('click', submitAction);
}