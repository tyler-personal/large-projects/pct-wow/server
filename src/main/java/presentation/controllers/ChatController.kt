package presentation.controllers

import io.javalin.embeddedserver.jetty.websocket.*
import j2html.TagCreator.*
import org.eclipse.jetty.websocket.api.Session
import org.json.JSONObject

object ChatController {
    private val nameBySession = mutableMapOf<Session, String>()

    fun join(ws: WebSocketHandler) {
        ws.onConnect { session ->
            val name = session.param("name") ?: "Anonymous"
            nameBySession[session] = name
            broadcastMessage("Server", "$name joined the chat")
        }
        ws.onClose { session, status, message ->
            val name = nameBySession[session] ?: "Anonymous"
            nameBySession.remove(session)
            broadcastMessage("Server", "$name left the chat")
        }
        ws.onMessage { session, message ->
            broadcastMessage(nameBySession[session] ?: "Anonymous", message)
        }
    }

    private fun broadcastMessage(sender: String, message: String) {
        nameBySession.keys.filter { it.isOpen }.forEach { session ->
            session.remote.sendString(JSONObject()
                .put("userMessage", article(b("$sender: "), span(message)).render())
                .put("userList", nameBySession.values).toString()
            )
        }
    }
}