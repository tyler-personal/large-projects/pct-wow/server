$(() => $('#name_modal').modal('show'));

function loadSockets() {
    loadGameSocket();
    loadChatSocket();
}

async function loadChatSocket() {
    let name = $("#name").val();

    let ws = new WebSocket("ws://" + location.hostname + ":" + location.port + "/chat/" + name);
    ws.onmessage = updateChat;
    ws.onclose = msg => loadChatSocket()
    let message = $("#message")[0];

    $("#send")[0].addEventListener("click", () => sendAndClear(message.value));
    message.addEventListener("keypress", (e) => {
        if (e.keyCode === 13) {
            sendAndClear(e.target.value);
        }
    });

    function sendAndClear(text) {
        if (text !== "") {
            ws.send(text);
            message.value = "";
        }
    }

    function updateChat(msg) {
        let data = JSON.parse(msg.data);
        $("#chat").after(data["userMessage"]);
        $("#user_list").html(data["userList"].map(user => "<li>" + user + "</li>").join(""));
    }
}

let nameToNumber = {}
let nameToCountdown = {}

async function loadGameSocket() {
    let id = $("#groupIDHeader").text()

    let ws = new WebSocket("ws://" + location.hostname + ":" + location.port + "/groups/" + id)
    ws.onclose = msg => loadGameSocket()

    ws.onmessage = msg => {
        let data = JSON.parse(msg.data);
        if (data["type"] == "newPlayer") {
            arr = data["nameRealm"].split("-")
            $("#name" + data["number"]).text(arr[0]);
            $("#realm" + data["number"]).text(arr[1]);
            nameToNumber[data["nameRealm"]] = data["number"];
        }
        else if (data["type"] == "newSpell") {
            // TODO: Abstract .split(" ").join(' ') and maybe other things (it's 5 AM xd)
            $.ajax({
                url: "https://us.api.battle.net/wow/spell/" + data["id"] + "?locale=en_US&apikey=pmqd9mhcmpek2zfdfnu975ht2rbxuwa7",
                headers: {"Access-Control-Allow-Origin": "*"},
                method: 'GET',
                success: wowAPIResponse => {
                    var ele = `
<span id="${data["name"].split(" ").join('_')}_${data["nameRealm"].split(" ").join('_')}" style="position: relative; text-align: center; color: white; width: 56px; height: 56px; font-size: 35px;">
  <img id="${data["name"].split(" ").join('_')}Image_${data["nameRealm"].split(" ").join('_')}" src="http://wow.zamimg.com/images/wow/icons/large/${wowAPIResponse.icon}.jpg" />
  <div id="${data["name"].split(" ").join('_')}Cooldown_${data["nameRealm"].split(" ").join('_')}" style="position: absolute; top: 20%; left: 50%; transform: translate(-50%, -50%); visibility: hidden">
    ${Math.round(data["cooldown"])}
  </div>
</span>
`
                    $("#spellList" + nameToNumber[data["nameRealm"]]).after(ele);
                }
            })
        }
        else if (data["type"] == "spellTriggered") {
            var image = $("#" + data["name"].split(" ").join('_') + "Image_" + data["nameRealm"].split(" ").join('_'));
            var cooldown = $("#" + data["name"].split(" ").join('_') + "Cooldown_" + data["nameRealm"].split(" ").join('_'));
            var c = parseInt(data["cooldown"]);
            image.css("opacity", ".5");
            cooldown.css("visibility", "visible");

            if (!(data["name"] in nameToCountdown)) {
                var countdown = setInterval(() => {
                    nameToCountdown[data["name"]] = countdown;
                    c = c - 1;
                    cooldown.text(c);

                    if (c < 1) {
                        image.css("opacity", "1");
                        cooldown.css("visibility", "hidden");
                        delete nameToCountdown[data["name"]];
                        clearInterval(countdown);
                        cooldown.text(Math.round(data["cooldown"]));
                    }
                }, 1000)
            }

        }

    };
}



