package domain.interactors

import java.util.*
import kotlin.streams.asSequence

object RandomNumberGenerator {
    val source = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    val random = Random()

    fun perform() = random.ints(10, 0, source.length).asSequence().map(source::get).joinToString("")

}