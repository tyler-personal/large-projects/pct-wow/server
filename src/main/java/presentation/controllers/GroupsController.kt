package presentation.controllers

import domain.entities.*
import domain.interactors.RandomNumberGenerator
import io.javalin.Context
import io.javalin.embeddedserver.jetty.websocket.*
import org.json.JSONObject
import viewLocation

object GroupsController {
    private val groupsByID = mutableMapOf<String, Group>()

    fun show(ctx: Context) {
        val id = ctx.param("id")
        val group = groupsByID[id]
        val params = mapOf("id" to id, "name" to group?.name)

        if (id != null && group != null)
            ctx.renderMustache("$viewLocation/groups.html", params)
        else
            ctx.renderMustache("$viewLocation/group_not_found.html")
    }

    fun join(ws: WebSocketHandler) {
        ws.onConnect { session ->
            val group = groupsByID[session.param("id")]
            group?.sessions?.add(session)
        }

        ws.onClose { session, status, message ->
            val group = groupsByID[session.param("id")]
            group?.sessions?.remove(session)

//            if (group?.sessions?.size == 0)
//                groupsByID.remove(session.param("id"))
            // Think about this, it's not good right now because of the consistent reconnects of the ws but later on it might be good to cleanup shit.
        }
    }

    fun create(ctx: Context) {
        val name = ctx.formParam("name") ?: "Unnamed Group"

        var id: String
        do {
         id = RandomNumberGenerator.perform()
        } while (groupsByID.containsKey(id))

        val group = Group(name, id)
        groupsByID[id] = group

        ctx.status(201)
        ctx.result("""{"id": "$id"}""")
    }

    fun checkIfGroupExists(ctx: Context) {
        val group = groupsByID[ctx.param("id") ?: ""]

        ctx.result("""{"exists": "${group != null}"}""")
    }

    fun trackNewSpell(ctx: Context) {
        val group = groupsByID[ctx.param("id") ?: ""]

        if (group != null) {
            val jObj = JSONObject().put("type", "newSpell")
            listOf("name", "id", "cooldown", "keybinding", "nameRealm").forEach {
                jObj.put(it, ctx.formParam(it) ?: "")
            }
            broadcastToGroup(group, jObj)
        }
    }

    fun trackNewPlayer(ctx: Context) {
        val group = groupsByID[ctx.param("id") ?: ""]

        if (group != null && group.players.size < 3) {
            val nameRealm = ctx.formParam("nameRealm") ?: ""
            val name = nameRealm.split("-")[0]
            val realm = nameRealm.split("-")[1]
            broadcastToGroup(group, JSONObject()
                .put("type", "newPlayer")
                .put("number", arrayOf("One", "Two", "Three")[group.players.size])
                .put("nameRealm", nameRealm)
            )
            group.players.add(Player(name, realm, mutableListOf()))
        }
    }

    fun spellTriggered(ctx: Context) {
        val group = groupsByID[ctx.param("id") ?: ""]

        if (group != null) {
            val jObj = JSONObject().put("type", "spellTriggered")
            listOf("name", "id", "cooldown", "keybinding", "nameRealm").forEach {
                jObj.put(it, ctx.formParam(it) ?: "")
            }
            broadcastToGroup(group, jObj)
        }
    }

    private fun broadcastToGroup(group: Group, jObj: JSONObject) {
        group.sessions.filter { it.isOpen }.forEach { session ->
            session.remote.sendString(jObj.toString())
        }
    }
}