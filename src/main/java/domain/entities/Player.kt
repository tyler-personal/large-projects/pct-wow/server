package domain.entities

class Player(val name: String, val realm: String, val spells: MutableList<Spell>) {
    val nameRealm: String = "$name-$realm"
}