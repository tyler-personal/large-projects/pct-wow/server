$(() => {
    if (window.location.protocol==="https") {
        alert("THIS OCCURRED!!");
        window.location.protocol="http";
    }
    setupModal('groupName', 'createModal', 'createGroupButton', 'createGroupSubmitButton', () => {
        $.post("/groups/create", {"name": $("#groupName").val()})
            .done(data => window.location.href = "/groups/" + JSON.parse(data)["id"])
    });

    setupModal('groupID', 'joinModal', 'joinGroupButton', 'joinGroupSubmitButton', () =>
        window.location.href = `groups/${$("#groupID").val()}`
    );
});

