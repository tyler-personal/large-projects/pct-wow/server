package domain.entities

import org.eclipse.jetty.websocket.api.Session

class Group(
    val name: String, val id: String,
    val sessions: MutableList<Session> = mutableListOf(), val players: MutableList<Player> = mutableListOf()
) {
    fun getPlayer(name: String, realm: String) = players.find { it.name == name && it.realm == realm }
}