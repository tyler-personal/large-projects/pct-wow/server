package domain.entities

class Spell(val name: String, val id: Int, val cooldown: Int, val keybinding: String, val nameRealm: String)