package presentation.controllers

import io.javalin.Context
import viewLocation

object MainController {
    fun index(ctx: Context) {
        ctx.renderMustache("$viewLocation/index.html")
    }
}